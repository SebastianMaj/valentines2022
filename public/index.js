$(window).on("load",function() {
    $(window).scroll(function() {
      var windowBottom = $(this).scrollTop() + $(this).innerHeight();
      $(".fade").each(function() {
        /* Check the location of each desired element */
        var objectBottom = $(this).offset().top + $(this).outerHeight();
        
        /* If the element is completely within bounds of the window, fade it in */
        if (objectBottom < windowBottom) { //object comes into view (scrolling down)
          if ($(this).css("opacity")==0) {$(this).fadeTo(500,1);}
        } else { //object goes out of view (scrolling up)
          if ($(this).css("opacity")==1) {$(this).fadeTo(500,0);}
        }
      });
    }).scroll(); //invoke scroll-handler on page-load

    var brd = document.createElement("DIV");
  document.body.insertBefore(brd, document.getElementById("board"));
  const duration = 3000;
  const speed = 0.5;
  const cursorXOffset = 0;
  const cursorYOffset = -5;
  var hearts = [];
  function generateHeart(x, y, xBound, xStart, scale)
  {
     var heart = document.createElement("DIV");
     heart.setAttribute('class', 'heart');
     brd.appendChild(heart);
     heart.time = duration;
     heart.x = x;
     heart.y = y;
     heart.bound = xBound;
     heart.direction = xStart;
     heart.style.left = heart.x + "px";
     heart.style.top = heart.y + "px";
     heart.scale = scale;
     heart.style.transform = "scale(" + scale + "," + scale + ")";
     if(hearts == null)
      hearts = [];
     hearts.push(heart);
     return heart;
  }

var before = Date.now();
var id = setInterval(frame, 5);
function frame()
{
   var current = Date.now();
   var deltaTime = current - before;
   before = current;
   for(i in hearts)
   {
    var heart = hearts[i];
    heart.time -= deltaTime;
    if(heart.time > 0)
    {
     heart.y -= speed;
     heart.style.top = heart.y + "px";
     heart.style.left = heart.x + heart.direction * heart.bound * Math.sin(heart.y * heart.scale / 30) + "px";
    }
    else
    {
     heart.parentNode.removeChild(heart);
     hearts.splice(i, 1);
    }
   }
}

var gr = setInterval(check, 400);
function check()
{
  var start = 1 - Math.round(Math.random()) * 2;
  var scale = Math.random() * Math.random() * 0.8 + 0.2;
  var bound = 30 + Math.random() * 20;
  generateHeart(100 + Math.random() * 150, 120, bound, start, scale);
}


  generateHeart(200, 120, null, null, 1);
  });